package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.Measurement;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int userID;
	private String vorname;
	private String nachname;
	private String geschlecht;
	private int alter;
	private String pwd;
	private String username;
	private String hypertonie;
	private String hypotonie;
	private String schwanger;
	private List<Measurement> measurements = new ArrayList<>();
	

	public User(int userID, String vorname, String nachname, String geschlecht, int alter , String username, 
			String pwd, String hypertonie, String hypotonie, String schwanger) {
		this.userID = userID;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geschlecht = geschlecht;
		this.alter = alter;
		this.username = username;
		this.pwd = pwd;
		this.hypertonie = hypertonie;
		this.hypotonie = hypotonie;
		this.schwanger = schwanger;

	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", username=" + username + ", pwd=" + pwd + ", vorname=" + vorname + ", nachname=" + nachname +"\n, hypertonie=" + hypertonie + ", hypotonie="
				+ hypotonie + ", alder=" + alter + ", geschlecht=" + geschlecht + ", schwanger=" + schwanger + ", measurements="
				+ measurements + "]";
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String isHypertonie() {
		return hypertonie;
	}

	public void setHypertonie(String hypertonie) {
		this.hypertonie = hypertonie;
	}
	
	public String getGeschelcht() {
		return geschlecht;
	}

	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}

	public String isHypotonie() {
		return hypotonie;
	}

	public void setHypotonie(String hypotonie) {
		this.hypotonie = hypotonie;
	}

	public String isSchwanger() {
		return schwanger;
	}

	public void setSchwanger(String schwanger) {
		this.schwanger = schwanger;
	}
	
	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}
}
