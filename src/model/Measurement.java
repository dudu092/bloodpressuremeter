package model;

import java.io.Serializable;
import java.util.*;
public class Measurement implements Serializable{
	private static final long serialVersionUID = 1L;
	
	int measurementID;
	int systolic;
	int diastolic;
	int pulse;
	Date date;
	
	public Measurement(int measurementID, int systolic, int diastolic, int pulse, Date date) {
		
		this.measurementID = measurementID;
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.pulse = pulse;
		this.date = date;
	}
	
	public Date getDate(){
		return date;
	}
	
	public void setDate(Date date){
		this.date = date;
	}
		
	public int getMeasurementID() {
		return measurementID;
	}
	public void setMeasurementID(int measurementID) {
		this.measurementID = measurementID;
	}
	public int getSystolic() {
		return systolic;
	}
	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}
	public int getDiastolic() {
		return diastolic;
	}
	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}
	public int getPulse() {
		return pulse;
	}
	public void setPulse(int pulse) {
		this.pulse = pulse;
	}
	
	public String getResultSys() {
		String ergebnisSystolisch = null;
		if (systolic < 120)
			ergebnisSystolisch = "optimal";
		if (systolic <= 130)
			ergebnisSystolisch = "normal";
		if (systolic > 130 && systolic <= 139)
			ergebnisSystolisch = "hochnormal";
		if (systolic >= 140 && systolic <= 159)
			ergebnisSystolisch = "Hypertonie Grad 1";
		if (systolic >= 160 && systolic <= 179)
			ergebnisSystolisch = "Hypertonie Grad 2";
		if (systolic >= 180)
			ergebnisSystolisch = "Hypertonie Grad 3";
		return ergebnisSystolisch;
	}

	public String getResultDiastolic() {
		String ergebnisDiastolisch = null;
		if (diastolic < 120)
			ergebnisDiastolisch = "optimal";
		if (diastolic <= 130)
			ergebnisDiastolisch = "normal";
		if (diastolic > 130 && diastolic <= 139)
			ergebnisDiastolisch = "hochnormal";
		if (diastolic >= 140 && diastolic <= 159)
			ergebnisDiastolisch = "Hypertonie Grad 1";
		if (diastolic >= 160 && diastolic <= 179)
			ergebnisDiastolisch = "Hypertonie Grad 2";
		if (diastolic >= 180)
			ergebnisDiastolisch = "Hypertonie Grad 3";
		return ergebnisDiastolisch;
	}

	public String getResultPulse() {
		String ergebnis=null;
		if(pulse >=60 && pulse <=80){
			ergebnis= "puls ist in Ordnung";
		}
		else if(pulse < 60){
			ergebnis = "bradykardie";
		}
		else if(pulse > 80){
			ergebnis = "Besuchen Sie einen Arzt";
		}
		
		return ergebnis;
	}
	@Override
	public String toString() {
		return "<br/>Measurement: \n\t systolic=" + systolic + ", diastolic=" + diastolic
				+ ", pulse=" + pulse + " \n <br/>ResultSys: " + getResultSys() +" \n ResultDiastolic: " + getResultDiastolic() +" \n ResultPulse: " + getResultPulse() + "\n Datum: " + getDate().toString() + "]";
	}

	
}
