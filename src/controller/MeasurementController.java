package controller;

import java.io.IOException;
import java.util.*;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bpm.MeasurementManagement;
import model.Measurement;
import model.User;

@WebServlet("/measurement/*")
public class MeasurementController extends HttpServlet {
	MeasurementManagement mmgmt = MeasurementManagement.instance;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<User> userAll = mmgmt.getUserList();
		req.setAttribute("users", userAll);
		System.out.println(mmgmt.getUserList());
		req.getRequestDispatcher("/WEB-INF/measurement.jsp").forward(req, resp);

	}

	int idcounter = (int) (System.currentTimeMillis() / 10000);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String username = req.getParameter("username");
		int userid = Integer.parseInt(req.getParameter("userid"));
		int systolic = Integer.parseInt(req.getParameter("systolic"));
		int diastolic = Integer.parseInt(req.getParameter("diastolic"));
		int pulse = Integer.parseInt(req.getParameter("pulse"));

		Date date = new Date();
		
		Measurement m = new Measurement(idcounter++, systolic, diastolic, pulse, date);

		mmgmt.addMessung(userid, m);
		
		HttpSession session = req.getSession();
		
		session.setAttribute("username", username);
		session.setAttribute("id", userid);
		
		session.setAttribute("systolic", systolic);
		session.setAttribute("diastolic", diastolic);
		session.setAttribute("pulse", pulse);
		session.setAttribute("date", date);
		
		 List<Measurement> messung = mmgmt.getUser(userid).getMeasurements();
	      //  List<Measurement> measurements = new ArrayList<Measurement>();
		session.setAttribute("measurement", messung);

		resp.sendRedirect(req.getContextPath() + "/user?id=" + userid);
	}
}
