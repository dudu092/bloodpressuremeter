package controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.apache.tomcat.jni.Mmap;

import bpm.MeasurementManagement;
import model.User;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/user/*")
public class UserController extends HttpServlet {
	MeasurementManagement mmgmt = MeasurementManagement.instance;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try{
			System.out.println("------");
			HttpSession session = request.getSession();
			int id = (int) session.getAttribute("id");
			System.out.println(id);
			User user = mmgmt.getUser(id);

			if (user == null) {
				response.sendError(404, "user not found");
				return;
			}

			request.setAttribute("user", user);
			
			System.out.println(user);
		
		}catch (NumberFormatException e) {
			response.sendError(404, "Es gab keine eingabe user!"); }
		
	//response.sendRedirect(request.getContextPath() + "/user");
		request.getRequestDispatcher("/WEB-INF/user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	int idcounter = (int) (System.currentTimeMillis() / 10000);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	String action = request.getParameter("action");
		
		if (action != null && action.equalsIgnoreCase("Eintragen")) {
            try {

                if (Registrieren(request, response) == false) {

                	response.sendError(404, "Falsche eingabe!");
                }
            } catch (ParseException e) {
                // TODO Auto-generated catch block
            	response.sendError(404, "Falsche eingabe!");
            } catch (NumberFormatException e) {
            	response.sendError(404, "Falsche eingabe!");
          }
            handleRequest(request, response);
        }
	}
		


	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + "/user");
		//response.sendRedirect("measurement");
	        
	 }
		
		
	private boolean Registrieren(HttpServletRequest request, HttpServletResponse response) 
				throws ParseException, ServletException, IOException {
		System.out.println("Registrieren!");
		//int idcounter = 0;
		idcounter++;
		int userID = idcounter;
		String vorname = request.getParameter("vorname");
		String nachname = request.getParameter("nachname");
		String username = request.getParameter("username");
		String pwd = request.getParameter("pwd");
		
		String geschlecht = request.getParameter("geschlecht");
		String hypertonie = request.getParameter("hypertonie");
		String hypotonie = request.getParameter("hypotonie");
		String schwanger = request.getParameter("schwanger");
		
		int day = (Integer) Integer.parseInt(request.getParameter("gueltTag"));
		int monat = (Integer) Integer.parseInt(request.getParameter("gueltMonat"));
		int year = (Integer) Integer.parseInt(request.getParameter("gueltJahr"));
		
		if(!verifydate(day, monat, year)){
			return false;
		}
		
		Calendar birthdate =  Calendar.getInstance();
		birthdate.set(year, monat, day);
		
		//Date date = new Date();
		int alter= age(birthdate);
		if(alter == -1){
			return false;
		}
		//Measurement bemessung = new Measurement (0, 0, 0, 0, date);
		if(vorname == null) vorname = " ";
		if(nachname == null) nachname = " ";
		
		if(username == null || username.trim().isEmpty())
			// nicht gesetzt oder leer --> konnte nicht eingeloggt werden
			return false;
		if(pwd == null || pwd.trim().isEmpty())
			// nicht gesetzt oder leer --> konnte nicht eingeloggt werden
			return false;
		if(geschlecht == null || hypotonie == null || hypertonie == null || schwanger == null)
			// nicht gesetzt oder leer --> konnte nicht eingeloggt werden
			return false;
		
		User user = new User(userID, vorname, nachname, geschlecht, alter , username, pwd, hypertonie, hypotonie, schwanger);
		mmgmt.addUser(user);
		System.out.println(user);
		
		HttpSession session = request.getSession();
		session.setAttribute("User", user);
		session.setAttribute("username", username);
		session.setAttribute("pwd", pwd);
		session.setAttribute("id", userID);
		
		session.setAttribute("vorname", vorname);
		session.setAttribute("nachname", nachname);
		session.setAttribute("alter", alter);
		
		
		session.setAttribute("geschlecht", geschlecht);
		session.setAttribute("hypertonie", hypertonie);
		session.setAttribute("hypotonie", hypotonie);
		session.setAttribute("schwanger", schwanger);
		
		System.out.println("User hinzugefuegt!");
		return true;
//		System.out.println("input bu:" + user);
		
		// doGet(request, response);
	}
	
	
	
	  public int age(Calendar birthdate) {
	        Calendar aktDatum = Calendar.getInstance();
	        int y, d, alter;
	 
	        //anderes Datum liegt vor Geburtsdatum
	        if (aktDatum.before(birthdate))
	            return -1;
	        
	        //Jahresunterschied berechnen
	        alter = aktDatum.get(Calendar.YEAR) - birthdate.get(Calendar.YEAR);
	        
	        //Prüfen, ob Tag in aktDatum vor Tag in birthdate liegt. Wenn ja, Alter um 1 Jahr vermindern
	        if ( (aktDatum.get(Calendar.MONTH) < birthdate.get(Calendar.MONTH))
	            ||(aktDatum.get(Calendar.MONTH) == birthdate.get(Calendar.MONTH)
	                && aktDatum.get(Calendar.DAY_OF_MONTH) < birthdate.get(Calendar.DAY_OF_MONTH)))
	            --alter;
	        
	        return alter;

	    }
	  
	  
	  public boolean verifydate(int Tag, int Monat, int Jahr){
		  if(Tag > 31 || Tag < 0) return false;
		  if(Monat > 12 || Monat < 1) return false;
		  if(Jahr > 2016) return false;
		  
		 return true;
		  
	  }

}

