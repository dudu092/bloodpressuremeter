package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bpm.MeasurementManagement;
import model.User;


/**
 * Servlet implementation class HomeController
 */
@WebServlet("/home/*")
public class HomeController extends HttpServlet {
	/**
	 * wir nutzen das MeasurementManagement, um spaeter logins und so durchfuehren zu koennen...
	 */
	MeasurementManagement mmgmt = MeasurementManagement.instance;

/*	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<User> userAll = mmgmt.getUserList();
		request.setAttribute("users", userAll);
		
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
*/
	
/*		
	/**
	 * Diese Methode wird einmalig aufgerufen, wenn das Servlet initialisiert wird.
	 /
	@Override
	public void init() throws ServletException 
	{
		// wir fuegen hier HardCoded einfach mal einen User hinzu
		User user = new User();
		user.setUsername("testuser");
		
		mmgmt.addUser(user);
	}
*/	
	/**
	 * Diese Methode wird aufgerufen, wenn der Browser einen GET-Request an Tomcat geschickt hat
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// als erstes behandeln wir alle Sachen, die wir in einem GET-Request machen muessen, z.B.:
		
		// ist das ein logout-request?
		String action = request.getParameter("action");
		
		// action ist logout
		if(action != null && action.equalsIgnoreCase("logout"))
			doLogout(request);

		// dann je nachdem, ob wir eingeloggt sind oder nicht, die richtige Seite anzeigen:
		handleRequest(request, response);
	}
	
	/**
	 * Diese Methode wird aufgerufen, wenn der Browser einen POST-Request an Tomcat geschickt hat
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// als erstes behandeln wir alle Sachen, die wir in einem POST-Request machen muessen, z.B.:
		
		// ist das ein login-request?
		String action = request.getParameter("action");
		
		if(action != null && action.equalsIgnoreCase("login"))
			doLogin(request);

		// dann je nachdem, ob wir eingeloggt sind oder nicht, die richtige Seite anzeigen:
		handleRequest(request, response);
	}
	
	/**
	 * Diese Methode wird von doGet und doPost aufgerufen, um Code der in beiden Methoden gleich wäre, hier auszulagern...
	 */
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if(isUserLoggedIn(request))
			request.getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
		else
			request.getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
		
		response.setContentType("text/html");
	}
	
	/**
	 * Diese Methode schaut, ob die aktuelle Anfrage eine Session besitzt und ob in dieser Session schon
	 * ein Username gespeichert ist, was bedeuten wuerde, dass der User eingeloggt ist.
	 * 
	 * @return <code>true</code> wenn der User eingeloggt ist, sonst <code>false</code>.
	 */
	private boolean isUserLoggedIn(HttpServletRequest request)
	{
		// Wir holen uns die Session von der aktuellen Anfrage
		HttpSession session = request.getSession(true);		// true = wenn die Anfrage keine Session hat (der User verbindet sich das erste mal), dann erstelle sofort eine fuer ihn!

		// wir holen den usernamen aus der Session heraus:
		String username = (String)session.getAttribute("username");
		
		// wurde der username vorher gesetzt??
		if(username == null)
			// nein -> der user ist nicht eingeloggt...
			return false;
		
		// der user ist eingeloggt, da die Variable username einen Wert hat
		return true;
	}
	
	/**
	 * Diese Methode versucht den User einzuloggen
	 */
	private boolean doLogin(HttpServletRequest request)
	{
		// wir holen uns die Parameter, welche das Formular geschickt hat:
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		// hier normalerweise auch das Passwort...
		
		// wir ueberpruefen, ob der username gesetzt wurde:
		if(username == null || username.trim().isEmpty())
			// nicht gesetzt oder leer --> konnte nicht eingeloggt werden
			return false;
		
		// ansonsten das UserManagement fragen, ob der User eingeloggt werden kann:
		if(mmgmt.getUserbyUsername(username).getUsername().equals(username) && mmgmt.getUserbyUsername(username).getPwd().equals(password))
		{
			// jup -> wir schreiben den Namen in die Session rein, als Signal dafuer, dass der User eingeloggt ist:
			HttpSession session = request.getSession();
			session.setAttribute("User", mmgmt.getUserbyUsername(username));
			session.setAttribute("username", username);
			session.setAttribute("password", password);
			return true;
		}
		
		return false;
	}
	
	/**
	 * diese methode loggt einen User aus, indem sie seine Session ungueltig macht
	 */
	private void doLogout(HttpServletRequest request)
	{
		// Wir holen uns die aktuelle Session von der Anfrage:
		HttpSession session = request.getSession(true);
		
		// und invalidieren sie:
		session.invalidate();
	}

}
