package bpm;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import model.Measurement;

import model.User;

public class MeasurementManagement {

	public static MeasurementManagement instance = new MeasurementManagement();

	private HashMap<Integer, User> db = new HashMap<>();

	public MeasurementManagement() {
		vonFestplatteLaden();

		if (db == null) {
			db = new HashMap<>();
			aufFestplatteSpeichern();
		}

	}

	private void aufFestplatteSpeichern() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/Users/fai12-1989/Desktop/DB/db.txt"));
			oos.writeObject(db);
			oos.close();
			System.out.println("ich komme hier rein und speichere alles");
		} catch (IOException e) {
			System.err.println("speichern error: " + e.getMessage());
		}
	}

	private void vonFestplatteLaden() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/Users/fai12-1989/Desktop/DB/db.txt"));
			db = (HashMap<Integer, User>) ois.readObject();
			ois.close();
		} catch (ClassNotFoundException | IOException e) {
			System.err.println("laden error: " + e.getMessage());
		}
	}

	public void addUser(User user) {

		if (db.containsKey(user.getUserID())) {
			System.out.println("id vergeben");
			throw new IllegalArgumentException("id bereits vergeben");
		}
		if (db.containsKey(user.getUsername())) {
			System.out.println("user vorhanden");
			throw new IllegalArgumentException("Username bereits vergeben");
		}
		if (db.containsKey(user.getPwd())) {
			System.out.println("pwd vergeben");
			throw new IllegalArgumentException("Password bereits vergeben");
		}
		System.out.println("ich werde es speichern");
		db.put(user.getUserID(), user);
		aufFestplatteSpeichern();

	}

	public void addMessung(int userID, Measurement measurement) {
		if (!db.containsKey(userID))
			throw new IllegalArgumentException("user not found");

		// db.get(benutzerID).addBemessung(bemessung);
		db.get(userID).getMeasurements().add(measurement);
		aufFestplatteSpeichern();

	}

	public User getUser(int id) {
		return db.get(id);
	}
	
	public User getUserbyUsername(String username) {
		
		if (db.containsKey(username)) {
			return db.get(username);
		}
	
		else {
			throw new IllegalArgumentException("Username nicht vorhanden! ");
		}
		
		
	}
	
	public int getUserID(String username){
		return db.get(username).getUserID();
	}
	
	public List<User> getUserList() {
		return new LinkedList<User>(db.values());
	}

}
