<%@page import="model.User"%>
<%@page import="model.Measurement"%>
<%@page import="bpm.MeasurementManagement"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Day', 'Systolic', 'Diastolic', 'Pulse'],
          ['07 h',  111,      62, 		 89],
          ['12 h',  120,      77, 		 88],
          ['18 h',  115,      65,  	 	 87],
          ['23 h',  125,      78,   	 89]
        ]);

        var options = {
          title: 'Blood Pressure',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    
    <jsp:include page="/WEB-INF/header2.jsp"></jsp:include>
    <br><br>
    <div id="curve_chart" style="width: 600px; height: 300px"></div>
    <br><br>
	<h1> Blutdruckdaten</h1>
<h2>${user.username}</h2>
<br>
<h3>Measurements</h3>


<%
User user = (User) request.getAttribute("user");
for(Measurement m:user.getMeasurements()){
	out.println(m+"\n");
}
%>
<br/>
<a href="<%=request.getContextPath() %>/measurement?userid=<%=user.getUserID() %>">
new entry for this person</a>
  </body>
</html>
