<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
 <link rel="stylesheet" href="<%=request.getContextPath()%>/css/styles.css">
</head>
<jsp:include page="/WEB-INF/header2.jsp"></jsp:include>
<h1> Blutdruckdaten</h1>
<body>

<form action="/measurement" method="post">
 
<input type="hidden" name="action" value="Eintragen">
userid: <input name="userid" value="<%=request.getParameter("userid") %>" disabled="true" />
<br/>
systolic: <input name="systolic"/>
<br/>
diastolic: <input name="diastolic"/>
<br/>
pulse: <input name="pulse"/>
<br/>
 <input type="submit" value="Eintragen"/>
</form>
 
</body>
</html>